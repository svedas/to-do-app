//
//  TO_DO_AppUITests.swift
//  TO-DO AppUITests
//
//  Created by Mantas Svedas on 1/14/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import XCTest

class TO_DO_AppUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAddingTask() {
        app.launch()

        
        let app = XCUIApplication()
        
        let newItem: String = "ABC"
        
        app.navigationBars["TO_DO_App.View"].buttons["Add"].tap()
        app.textFields["Enter task header"].tap()
        app.textFields["Enter task header"].typeText(newItem)
        app.navigationBars["TO_DO_App.AddTaskView"].buttons["Save"].tap()
        let isNewItem = app.tables.staticTexts[newItem].exists
        
        XCTAssert(isNewItem)
        
        app.terminate()
    }
    
    func testAddingTaskWithDesc() {
        app.launch()

        
        let app = XCUIApplication()
        
        let newItem: String = "ABC"
        let descText: String = "aaaaaaaaaa"
        
        app.navigationBars["TO_DO_App.View"].buttons["Add"].tap()
        app.textFields["Enter task header"].tap()
        app.textFields["Enter task header"].typeText(newItem)
        
        app.textFields["Enter task details"].tap()
        app.textFields["Enter task details"].typeText(descText)
        
        app.navigationBars["TO_DO_App.AddTaskView"].buttons["Save"].tap()
        let isNewItem = app.tables.staticTexts[newItem].exists
        
        XCTAssert(isNewItem)
        
        app.terminate()
    }
    
    func testEditingTask() {
        app.launch()
        
        let newItem: String = "ABC"
        let moreText: String = "abcc"

        app.navigationBars["TO_DO_App.View"].buttons["Add"].tap()
        let enterTaskHeaderTextField = app.textFields["Enter task header"]
        enterTaskHeaderTextField.tap()
        enterTaskHeaderTextField.typeText(newItem)
        let saveButton = app.navigationBars["TO_DO_App.AddTaskView"].buttons["Save"]
        saveButton.tap()

        app.tables.staticTexts[newItem].tap()

        let enterTaskDetailsTextField = app.textFields["Enter task details"]
        enterTaskDetailsTextField.tap()
        enterTaskDetailsTextField.typeText(moreText)
        saveButton.tap()
        
        app.tables.staticTexts[newItem].tap()
        let isEditedItem = app.textFields[moreText].exists
        
        XCTAssertTrue(isEditedItem)
        
        app.terminate()
    }

//    func testLaunchPerformance() {
//        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
//            // This measures how long it takes to launch your application.
//            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
//                XCUIApplication().launch()
//            }
//        }
//    }
}
