//
//  Task.swift
//  TO-DO App
//
//  Created by Mantas Svedas on 1/8/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import Foundation

class Task {
    
    var header: String = ""
    var content: String = ""
    var isChecked: Bool = false
    
    init(header: String){
        self.header = header
        self.content = ""
        self.isChecked = false
    }
    
    init(header: String, content: String){
        self.header = header
        self.content = content
        self.isChecked = false
    }
    
}
