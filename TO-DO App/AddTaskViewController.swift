//
//  AddTaskViewController.swift
//  TO-DO App
//
//  Created by Mantas Svedas on 1/7/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import UIKit

protocol AddTask: class {
    func addTask(header: String)
    func addTask(header: String, content: String)
}

class AddTaskViewController: UIViewController {

    var task: Task?
    
    @IBOutlet weak var taskHeader: UITextField! // weak?
    @IBOutlet weak var taskContent: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Loads task info
        if let task = task {
            taskHeader.text = task.header
            taskContent.text   = task.content
        }
    }
    
    weak var delegate: AddTask?
    
    
    @IBAction func addTaskAction(_ sender: Any) {
        
        
        // Displaying error if empty
        if taskHeader.text?.isEmpty ?? false {                   // will this crash?
            let message = "Please write a task header"
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            self.present(alert, animated: true)

            // duration in seconds
            let duration: Double = 3

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
            }
        } else if taskContent.text == "" {
            delegate?.addTask(header: taskHeader.text!)
            navigationController?.popViewController(animated: true)
//            print("Added only header task")
        } else {
            delegate?.addTask(header: taskHeader.text!, content: taskContent.text!)
            navigationController?.popViewController(animated: true)
//            print("Added header/content task")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

}
