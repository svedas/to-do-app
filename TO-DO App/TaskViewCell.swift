//
//  TaskViewCell.swift
//  TO-DO App
//
//  Created by Mantas Svedas on 1/7/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import UIKit

protocol CheckBox: class {
    func switchCheckBox(state: Bool, index: Int)
}

class TaskViewCell: UITableViewCell {
    
    weak var delegate: CheckBox?
    var isChecked: Bool = false
    var index: Int?
    
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet weak var checkBoxButton: UIButton!
    
    @IBAction func changeCheckBox(_ sender: UIButton) {
        isChecked = !isChecked
        delegate?.switchCheckBox(state: isChecked, index: index ?? 0)
//        print("Check pressed: \(isChecked)")
        
    }
    
}
