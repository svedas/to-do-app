//
//  ViewController.swift
//  TO-DO App
//
//  Created by Mantas Svedas on 1/7/20.
//  Copyright © 2020 Mantas Svedas. All rights reserved.
//

import UIKit
import os.log

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AddTask, CheckBox {
    
    @IBOutlet var myTableView: UITableView!
    
    // Tasks array
    var tasks: [Task] = []
    
    // Methond running when view is loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        // Testing adding tasks
//        tasks.append(Task(header: "Test"))
//        tasks.append(Task(header: "One", content: "1"))
//        tasks.append(Task(header: "Two"))
//        tasks.append(Task(header: "Three", content: "3"))
        
    }
    
    // Maybe used when switching views?
    override func viewDidAppear(_ animated: Bool) {
        myTableView.reloadData()
    }
    
    // Returns table cells count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    // Shows table cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "taskCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! TaskViewCell // checks identifier
            
        // fills the cell with information
        cell.headerLabel.text = tasks[indexPath.row].header
        cell.isChecked = tasks[indexPath.row].isChecked
        cell.index = indexPath.row
        cell.delegate = self
        
        if tasks[indexPath.row].isChecked == true {
            let image = UIImage(systemName: "checkmark.square")
            cell.checkBoxButton.setImage(image, for: UIControl.State.normal)
        } else {
            let image = UIImage(systemName: "square")
            cell.checkBoxButton.setImage(image, for: UIControl.State.normal)
        }
        return cell
    }
    
    // Enables deletion when swiping to the left
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath){
        if editingStyle == UITableViewCell.EditingStyle.delete {
            tasks.remove(at: indexPath.row)
            myTableView.reloadData()
        }
    }

    
    // Important stuff, checks segue identifier and tells next view where control is comming from
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch(segue.identifier ?? "") {
        case "AddButton":
            os_log("Adding a new task.", log: OSLog.default, type: .debug)
            let nextViewController = segue.destination as! AddTaskViewController
            nextViewController.delegate = self
            
        case "ShowDetail":
            os_log("Showing an existing task.", log: OSLog.default, type: .debug)
            let nextViewController = segue.destination as? AddTaskViewController
            
            guard let selectedTaskCell = sender as? TaskViewCell else {
                return
            }
            
            let indexPath = myTableView.indexPath(for: selectedTaskCell)
            
            let selectedTask = tasks[indexPath!.row]
            nextViewController?.task = selectedTask
            nextViewController?.delegate = self
            
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier ?? "Something unexpected happened")")

        }
    }
    

}

extension ViewController {
    // Method implementing AddTask protocol, add or edits the task
    func addTask(header: String) {
        if let selectedIndexPath = myTableView.indexPathForSelectedRow {
            // Updates an existing task.
            tasks[selectedIndexPath.row].header = header
            myTableView.reloadRows(at: [selectedIndexPath], with: .none)
        } else {
            tasks.append(Task(header: header))
            myTableView.reloadData()
        }
    }
    func addTask(header: String, content: String) {
        if let selectedIndexPath = myTableView.indexPathForSelectedRow {
            // Updates an existing task.
            tasks[selectedIndexPath.row].header = header
            tasks[selectedIndexPath.row].content = content
            myTableView.reloadRows(at: [selectedIndexPath], with: .none)
        } else {
            tasks.append(Task(header: header, content: content))
            myTableView.reloadData()
        }
    }
}

extension ViewController {
    func switchCheckBox(state: Bool, index: Int) {
        // Updates task check box.
        tasks[index].isChecked = state
        myTableView.reloadData()
    }
}



